const express = require('express')
const app = express()
const port = 3000

/**
 * @param a = number
 * @param b = number
 * @returns sum of a and b
 */
const add = (a, b) => {
  return a+b
}

app.get('/add/', (req, res) => {
  const x = add(1,7)
  res.send(`Sum: ${x}`)
})

app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})